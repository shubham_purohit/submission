var search_select = document.getElementById("search");
var search_input = document.getElementById("s_input");

var search_list = [];

// search box keydown(monitor) listener
search_input.addEventListener("keydown", function() {
    var select_option = search_select.options[search_select.selectedIndex].text;

    // clear list for every new search key
    search_list.splice(0, search_list.length);

    // updated new search list result
    search_list = search_Handler(select_option);

    if (search_list.length > 0) {
        load_BookList(search_list);
    } else {
        // search box not empty
        if (search_input.value != "") {
            empty_section();
            display(error_no_data);
        } else {
            //search box empty
            load_BookList(response);
        }
    }
});

// handler for search based on drop down selection
function search_Handler(select_option) {
    if (search_input.value != "") {
        if (select_option == "Name") {
            for (var book of response) {
                if (book.name.toLowerCase().search(search_input.value.toLowerCase()) != -1) {
                    search_list.push(book);
                }
            }
        } else if (select_option == "Author") {
            for (var book of response) {
                if (book.author.toLowerCase().search(search_input.value.toLowerCase()) != -1) {
                    search_list.push(book);
                }
            }
        } else if (select_option == "Genre") {
            for (var book of response) {
                if (book.genre.toLowerCase().search(search_input.value.toLowerCase()) != -1) {
                    search_list.push(book);
                }
            }
        } else if (select_option == "Book Id") {
            for (var book of response) {
                if (book.id.search(search_input.value) != -1) {
                    search_list.push(book);
                }
            }
        }
    } else if (search_input.value == "") {
        // clear list for every new search key
        //search_list.splice(0, search_list.length);
    }

    return search_list;
}