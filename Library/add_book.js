// common button for add/update button
var add_button = document.getElementById("add_button");

// variable for add layout
var a_book_name = document.getElementById("a_book_name");
var a_author = document.getElementById("a_author");
var a_genre = document.getElementById("a_genre");
var a_id = document.getElementById("a_id");
var a_image = document.getElementById("a_image");


// layout toggle
add_books.addEventListener('click', function() {
    display(add);
    empty_fields();
});

// add button handling
add_button.addEventListener("click", function() {

    // create new book object from the inputs
    create_object();
    response = JSON.parse(localStorage.getItem("books"));

    console.log(response);

    if (response && validate(response, 1)) {
        response.push(book);
        localStorage.setItem("books", JSON.stringify(response));
        alert("Book added successfully");
        load_BookList(response);
    }
});

function create_object() {
    book.name = a_book_name.value;
    book.author = a_author.value;
    book.genre = a_genre.value;
    book.id = a_id.value;
}

function empty_fields() {
    a_book_name.value = "";
    a_author.value = "";
    a_genre.value = "";
    a_id.value = "";
}