// variable for update layout
var u_book_name = document.getElementById("u_book_name");
var u_author = document.getElementById("u_author");
var u_genre = document.getElementById("u_genre");
var u_image = document.getElementById("u_image");

var update_button = document.getElementById("update_button");

var update_obj;
var updated_image;

function edit_handler(item) {
    display(update);
    alert("If no image provided on update page, previously stored image will be set!");
    update_obj = item;
    update_inputs(item);
}

function update_inputs(item) {
    u_book_name.value = item.name;
    u_author.value = item.author;
    u_genre.value = item.genre;
}

update_button.addEventListener("click", function() {
    response = JSON.parse(localStorage.getItem("books"));
    //console.log(response);

    if (response && validate(response, 2)) {
        response = update_Local_Storage(response);

        localStorage.setItem("books", JSON.stringify(response));
        alert("Book Updated Successfully");
        load_BookList(response);
    }
});

function update_Local_Storage(response) {

    for (var item of response) {
        if (item.id == update_obj.id) {
            item.name = u_book_name.value;
            item.author = u_author.value;
            item.genre = u_genre.value;
            if (updated_image) {
                item.image = updated_image;
            }
        }
    }
    return response;
}