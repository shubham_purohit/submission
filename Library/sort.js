var sort_select = document.getElementById("sort");
var sort_list = [];

// sort select dropdown event listener
sort_select.addEventListener("change", function() {
    var sort_option = sort_select.options[sort_select.selectedIndex].text;
    console.log(sort_option);

    if (sort_option != "None") {
        // copy raw array contents to new array to be sorted
        sort_list = response.slice();
        sort_list = sort_Handler(sort_option);
    }
    if (sort_list.length > 0) {
        load_BookList(sort_list);
    } else {
        load_BookList(response);
    }
});

// sort handler based on drop down selection
function sort_Handler(sort_option) {

    if (sort_option == "Name") {
        sort_list.sort(function compare(a, b) {
            const str1 = a.name.toLowerCase();
            const str2 = b.name.toLowerCase();

            var result = 0;
            result = str1 > str2 ? 1 : -1;
            return result;
        });

    } else if (sort_option == "Author") {
        sort_list.sort(function compare(a, b) {
            const str1 = a.author.toLowerCase();
            const str2 = b.author.toLowerCase();

            var result = 0;
            result = str1 > str2 ? 1 : -1;
            return result;
        });
    } else if (sort_option == "Genre") {
        sort_list.sort(function compare(a, b) {
            const str1 = a.genre.toLowerCase();
            const str2 = b.genre.toLowerCase();

            var result = 0;
            result = str1 > str2 ? 1 : -1;
            return result;
        });
    } else if (sort_option == "None") {
        sort_list.splice(0, sort_list.length);
    }

    return sort_list;
}