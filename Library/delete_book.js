function delete_handler(item) {
    response = JSON.parse(localStorage.getItem("books"));
    //console.log(response);

    response = delete_book(response, item);

    // udpate the new list of books in the local storage
    localStorage.setItem("books", JSON.stringify(response));
    alert("Book Deleted Successfully");
    load_BookList(response);
}

// delete the item from the reponse array
function delete_book(response, item) {
    // remove the particular book from local storage
    for (var book of response) {
        if (book.id == item.id) {
            var index = response.indexOf(book);
            alert(index);
            if (index > -1) {
                response.splice(index, 1);
            }
        }
    }
    return response;
}