// validate inputs at add/update page
function validate(response, flag) {
    if (flag == 1) {
        if (!a_book_name.value || !a_author.value || !a_genre.value || !a_id.value || !a_image.value) {
            alert("Fields Empty. All fields are mandatory");
            return false;
        }

        if (a_id.value.length != 6) {
            alert("Book ID should be six digits");
            return false;
        }

        for (var item of response) {
            if (a_id.value == item.id) {
                alert("Book already present with same ID");
                return false;
            }
        }
    } else {
        console.log(u_book_name.value);
        console.log(u_genre.value);
        console.log(u_genre.value);

        if (!u_book_name.value || !u_author.value || !u_genre.value) {
            alert("Fields Empty. All fields are mandatory");
            return false;
        }
    }

    return true;
}

function empty_section() {
    while (view.firstChild) {
        view.removeChild(view.firstChild);
    }
}

function create_card(item) {
    var card = document.createElement("div");
    card.style.width = "fit-content";
    card.style.margin = "20px";
    card.style.display = "inline-block"
    card.style.margin = "20px"
    card.style.setProperty("border-radius", "5px")
    card.style.setProperty("box-shadow", "0 4px 8px 0 rgba(0, 0, 0, 0.2)");

    var image_cover = new Image;
    image_cover.style.width = "200px"
    image_cover.style.height = "280px"
    image_cover.src = item.image;

    var container = document.createElement("div");
    var h_name = document.createElement("h4");
    var p_author = document.createElement("p");
    var p_genre = document.createElement("p");
    var p_id = document.createElement("p");

    container.style.textAlign = "center";
    container.style.padding = "2px 16px";
    container.style.width = "auto"

    h_name.innerHTML = item.name;
    h_name.style.fontWeight = "bold";
    h_name.style.width = "150px"
    p_author.innerHTML = item.author;
    p_genre.innerHTML = item.genre;
    p_id.innerHTML = item.id;

    // edit button on each card
    var image_edit = new Image;
    // handler for edit button 
    image_edit.addEventListener("click", function() {
        edit_handler(item);
    });
    image_edit.style.width = "20px"
    image_edit.style.height = "20px"
    image_edit.src = "images/edit_icon.png";

    // delete button on each card
    var image_delete = new Image;
    // handler for delete button
    image_delete.addEventListener("click", function() {
        delete_handler(item);
    });
    image_delete.style.width = "20px"
    image_delete.style.height = "20px"
    image_delete.style.marginLeft = "40px"
    image_delete.src = "images/delete_icon.png";

    container.appendChild(h_name);
    container.appendChild(p_author);
    container.appendChild(p_genre);
    container.appendChild(p_id);
    container.appendChild(image_edit)
    container.appendChild(image_delete)

    card.appendChild(image_cover);
    card.appendChild(container)

    view.appendChild(card);
}

// file input handling - common for add and update
function onChange(event) {
    file = event.target.files[0];

    // a default file option can be implemented here
    var reader = new FileReader();
    reader.onload = function(e) {
        // file converted to base64 string
        var result = e.target.result

        console.log(result);

        // update the book object
        book.image = result;

        // update the update_image variable
        updated_image = result;
    };

    reader.readAsDataURL(file);
}

function display(mode) {
    console.log(mode);
    switch (mode) {
        case 0:
            view.style.display = "block";
            add_layout.style.display = "none";
            update_layout.style.display = "none";
            break;
        case 1:
            add_layout.style.display = "block";
            update_layout.style.display = "none";
            view.style.display = "none";
            break;
        case 2:
            update_layout.style.display = "block";
            add_layout.style.display = "none";
            view.style.display = "none";
            break;
        case 3:
            view.style.display = "block";
            add_layout.style.display = "none";
            update_layout.style.display = "none";

            var image = new Image
            image.src = "images/no_data_back.png";
            image.style.width = "100%";
            view.appendChild(image);
            break;
    }
}