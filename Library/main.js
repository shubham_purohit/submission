// tabs
var view_books = document.getElementById("view_books");
var add_books = document.getElementById("add_books");
var frame = document.getElementById("frame");

// sections
var view = document.getElementById("view");
var add_layout = document.getElementById("add_layout");
var update_layout = document.getElementById("update_layout");

// file read variable
var file = null;

// dummy object
var book = {
    "name": "dummy",
    "author": "dummy",
    "genre": "dummy",
    "id": "dummy",
    "image": ""
}

// fetch from the local storage
var response = JSON.parse(localStorage.getItem("books"));
if (response == undefined || response == null) {
    var books = [];
    localStorage.setItem("books", JSON.stringify(books));
}

// default page display at view books
load_BookList(response);

/*--------------- view books tab code ---------------- */

view_books.addEventListener('click', function() {
    load_BookList(response);
});

function load_BookList(response) {
    // empty the container first
    empty_section();
    if (response != null && response.length > 0) {
        // toggle displays(sections in frame layout)
        display(book_list);

        //run the loop and append the book cards into the section
        for (var item of response) {
            create_card(item);
        }
    } else {
        display(error_no_data);
        //alert("No books in the database");
    }
}