var paratext = document.getElementById("text");

// button based alert
document.getElementById("button1").addEventListener("click", function() {
    alert("Hello World!")

    document.getElementById("para").textContent += "Hello"
});


// mouse based
paratext.addEventListener("mouseenter", function() {
    paratext.style.backgroundColor = "orange";
    paratext.style.color = "blue"
    paratext.style.fontSize = "30px"
})

document.getElementById("text").addEventListener("mouseleave", function() {

    paratext.style.backgroundColor = "white";
    paratext.style.fontSize = "20px"
})


paratext.addEventListener("click", function() {

    document.getElementById("para").textContent = "This is another paragraph in additon to the previous one"
})

var ipt = document.getElementById("inpt1")

ipt.addEventListener('change', function() {
    ipt.style.color = "red";
    ipt.style.fontStyle = "Italic"

})