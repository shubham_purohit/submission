var submit_button = document.getElementById("submit_button");
var print_button = document.getElementById("print_button");
var sign_in = document.getElementById("sign_in_button");


var fst_name = document.getElementById("first_name");
var snd_name = document.getElementById("second_name");
var mob_no = document.getElementById("mobile_no")
var empid = document.getElementById("emp_id")
var pass = document.getElementById("password")

console.log("hello");
var user = {
    "f_name": "",
    "s_name": "",
    "mobile": "",
    "empid": "",
    "password": ""
}

var response = localStorage.getItem("login_details");
if (response == undefined || response == null) {
    var temp = [];
    // default empty array at localstorage
    localStorage.setItem("login_details", JSON.stringify(temp));
}

function create_object() {
    user.f_name = fst_name.value;
    user.s_name = snd_name.value;
    user.mobile = mob_no.value;
    user.empid = empid.value;
    user.password = pass.value;
}

function validate(user_details) {

    if (!fst_name.value || !snd_name.value || !mob_no.value || !pass.value || !empid.value) {
        alert("Fields Empty. All fields are mandatory");
        return false;
    }

    for (var no of user_details) {
        if (user.mobile == no.mobile) {
            alert("User already present with the same mobile no");
            return false;
        }
    }

    if (user.mobile.length != 10) {
        alert("Mobile no should be 10 digit long");
        return false;
    }

    if (!(user.password.match(/[0-9]+/g))) {
        alert("Password must contain one number")
        return false;
    }

    return true;
}

submit_button.addEventListener("click", function() {

    // new object from the details entered
    create_object();

    //console.log(user);

    var response = localStorage.getItem("login_details");
    var UserDetails = JSON.parse(response);

    console.log(UserDetails);

    if (UserDetails != undefined && UserDetails != null) {
        if (validate(UserDetails)) {

            UserDetails.push(user);
            localStorage.setItem("login_details", JSON.stringify(UserDetails))

            alert("User added succesfully");
            location.replace("sign_in.html")
        }
    }

});

print_button.addEventListener("click", function() {
    var response = JSON.parse(localStorage.getItem("login_details"));

    console.log(response);
});

sign_in.addEventListener("click", function() {
    location.replace("sign_in.html")
})