var login_button = document.getElementById("login_button");
var sign_up = document.getElementById("sign_up_button");
var fst_name = document.getElementById("login_f_name");
var mob_no = document.getElementById("login_mobile_no")
var pass = document.getElementById("login_password")

console.log("hello");

// validation for user login 
function validate(response) {

    // empty fields length check
    if (!fst_name.value || !mob_no.value || !pass.value) {
        alert("Fields Empty. All fields are mandatory");
        return false;
    }

    // mobile no length check
    if (mob_no.value.length != 10) {
        alert("Mobile no should be 10 digit long");
        return false;
    }

    var flag = 0;
    var index = 0;

    // search for user mobile no
    for (var user of response) {
        if (mob_no.value == user.mobile) {
            flag = 1;
            break;
        }
        index++;
    }

    if (!flag) {
        alert("No user with this mobile no!");
        return false;
    }

    console.log(index)
    console.log(response[index].password)
    console.log(response[index].f_name)

    // password check
    if (flag) {
        if (pass.value != response[index].password || fst_name.value != response[index].f_name) {
            alert("Enter Correct Credentials\nEither password or username is incorrect");
            return false;
        }
    }

    return true;
}

// login button even handling
login_button.addEventListener("click", function() {
    var response = JSON.parse(localStorage.getItem("login_details"));

    console.log(response);

    if (response != undefined) {
        if (validate(response)) {
            location.replace("index.html")
        }
    }
});


// sign up button redirtecting
sign_up.addEventListener("click", function() {
    location.replace("sign_up.html")
})