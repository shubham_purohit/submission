//to display value 
function display(val) {
    document.getElementById('result').value += val;
}


//to evaluate the  digits 
function solve() {
    let x = document.getElementById('result').value;

    // create tokens based on delimeter(operators)
    //  array of tokens
    var numbers = x.split(/[\/, +, \-, *]+/);
    var oper = x.split(/[0-9\s]/g);

    let res = 0,
        idx = 0,
        flag = 1,
        ind = 0;
    var error = 0;

    // handling negative numbers
    if (oper[0] == "-") {
        numbers[1] = "-" + numbers[1];
        idx++;
        ind++;
        res = parseFloat(numbers[1]);
        flag = 0;
    }

    // loop to search for operators and subsequent calculation
    for (; ind < oper.length; ind++) {
        if (oper[ind] != "") {
            switch (oper[ind]) {
                case '+':
                    if (flag) {
                        res = parseFloat(numbers[idx]) + parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = parseInt(res) + parseInt(numbers[++idx]);
                    break;
                case '-':
                    if (flag) {
                        res = parseFloat(numbers[idx]) - parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = parseInt(res) - parseInt(numbers[++idx]);

                    break;
                case '*':
                    if (flag) {
                        res = parseFloat(numbers[idx]) * parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = parseInt(res) * parseInt(numbers[++idx]);

                    break;
                case '/':
                    if (flag) {
                        res = parseFloat(numbers[idx]) / parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = parseInt(res) / parseInt(numbers[++idx]);

                    break;
                default:
                    if (oper[ind].match(/[^*/+\-]*/g)) {
                        alert("Invalid Expression");
                        error = 1;
                    }
            }
            if (error)
                break;
        }
    }

    console.log(numbers);
    console.log(oper);
    console.log(res);

    if (!error) {
        document.getElementById('result').value = res;
    }
}

//clear the display on press of c button
function clr() {
    document.getElementById("result").value = "";
}